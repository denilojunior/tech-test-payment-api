using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.DTOs
{
    public class VendaDto
    {
        public int VendaId { get; set; }
        public DateTime Data { get; set; }
        public ProdutoDto ProdutoDto { get; set; }

        public EnumVenda StatusVenda { get; set; }
        
    }
}