using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTOs
{
    public class VendedorDto
    {
        public int VendedorId { get; set; }
        public string Nome { get; set; }
        
    }
}