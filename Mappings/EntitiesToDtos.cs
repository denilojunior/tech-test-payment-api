using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Models;
using tech_test_payment_api.DTOs;

namespace tech_test_payment_api.Mappings
{
    public class EntitiesToDtos : Profile
    {
        public EntitiesToDtos()
        {
            CreateMap<Venda, VendaDto>().ReverseMap();
        }
    }
}