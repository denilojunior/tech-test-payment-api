using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int VendaId { get; set; }
        public DateTime Data { get; set; }
        public EnumVenda StatusVenda { get; set; }

        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }

        public Produto Produto { get; set; }
    
    }
}