using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Produto
    {
       [JsonIgnore]
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }

        [JsonIgnore]
        public int VendaId { get; set; }

    }
}